# first commit
# Setup Python environment 

Description of the project goes here

1. First step to describe this
2. Step by step process

## Getting Started

Before you get started [read the original blog post](https://medium.com/@jwdobken/python-django-with-docker-and-gitlab-ci-b83cc4e7e2e) for an overview of the processes which can be done after this initial setup.

### Prerequisites

The following prerequisites are required:

1. An account with [Anaconda](https://www.anaconda.com/).
2. An account with [Dockerhub](https://hub.docker.com/).


### Install local software

Download and install docker per web instructions

Download and install anaconda per web instructions

### Make/Get SSH key for gitlab

[GitLab SSH Key documentation](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair)

### Create python environment

Within terminal:

Confirm anaconda installation worked...

```
$ conda info
```

![Spoonacular](screenshots/spoonacular3.png?rev=2&raw=true)

Create a new environment with python:

```
$ conda create -n testenv python=3.6
```

![Spoonacular](screenshots/spoonacular3.png?rev=2&raw=true)
![Spoonacular](screenshots/spoonacular3.png?rev=2&raw=true)

Activate the new environment 

```
$ conda activate testenv
```

![Spoonacular](screenshots/spoonacular3.png?rev=2&raw=true)

Confirm all python dependencies were installed:
```
$ conda list
```

![Spoonacular](screenshots/spoonacular3.png?rev=2&raw=true)


## Next Steps 

#Install a new Pythin project using cookiecutter

pip install "cookiecutter>=1.4.0"
cookiecutter https://github.com/pydanny/cookiecutter-django


For more information on how the school-agent bot works [read the original blog post](https://medium.com/ibm-watson-developer-cloud/how-to-build-a-recipe-slack-bot-using-watson-conversation-and-spoonacular-api-487eacaf01d4#.i0q8fnhuu).

We will be publishing a new blog post soon that will discuss the enhancements we have made to the original application, including
how we are using Cloudant to store chat history and enable the new "favorites" intent.


## Fute state 

#Add a ibm cloud cookiecutter 

https://dezoito.github.io/2016/05/11/django-gitlab-continuous-integration-phantomjs.html
## Privacy Notice

This application includes code to track deployments to [IBM Bluemix](https://www.bluemix.net/) and other Cloud Foundry platforms. The following information is sent to a [Deployment Tracker](https://github.com/cloudant-labs/deployment-tracker) service on each deployment:

* Application Name (`application_name`)
* Space ID (`space_id`)
* Application Version (`application_version`)
* Application URIs (`application_uris`)

This data is collected from the `VCAP_APPLICATION` environment variable in IBM Bluemix and other Cloud Foundry platforms. This data is used by IBM to track metrics around deployments of sample applications to IBM Bluemix to measure the usefulness of our examples, so that we can continuously improve the content we offer to you. Only deployments of sample applications that include code to ping the Deployment Tracker service will be tracked.

## License

Licensed under the [Apache License, Version 2.0](LICENSE.txt).
